/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo;

/**
 *
 * @author Jesus alberto venegas lopez
 */
public class RegistroVenta {
   
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private float horas;

    public RegistroVenta() {
        this.numDocente = 0;
        this.nombre = "";
        this.domicilio = "";
        this.nivel = 0;
        this.pagoBase = 0.0f;
        this.horas = 0.0f;
        
    }

    public RegistroVenta(int codigoVenta, int cantidad, int tipo, float precio) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horas = horas;
    }
    public RegistroVenta(RegistroVenta otro) {
        this.numDocente = otro.numDocente;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.nivel = otro.nivel;
        this.pagoBase = otro.pagoBase;
        this.horas = otro.horas;
    }

public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }
    
    
   public float calcularPago() {
    float pago = 0;
    if (this.nivel == 1) {
        pago = this.pagoBase * 1.30f; 
    } else if (this.nivel == 2) {
        pago = this.pagoBase * 1.50f; 
    } else if (this.nivel == 3) {
        pago = this.pagoBase * 2.0f; 
    }
    return pago * this.horas; 
}

public float calcularImpuesto() {
    return calcularPago() * 0.16f; 
}

public float calcularBono(int hijo) {
    float bono = 0.0f;
    float pagoTotal = calcularPago(); 
    if (hijo >= 1 && hijo <= 2) {
        bono = pagoTotal * 0.05f; 
    } else if (hijo >= 3 && hijo <= 5) {
        bono = pagoTotal * 0.10f; 
    } else if (hijo > 5) {
        bono = pagoTotal * 0.20f;
    }
    return bono;
}
 public float calcularTotal(int hijo) {
    float total = calcularPago() - calcularImpuesto() + calcularBono(hijo);
    return total;
}

}

  